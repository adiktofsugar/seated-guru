server {
	server_name {{ server_name }};
	listen 8080;

	location /media/{{ media_version }}/ {
		rewrite "/media/{{ media_version }}/(.*)" "/$1" break;
		root {{ project_root }}/target/;
	}

	location / {
		rewrite "(.*?)\.html$" "$1";
		rewrite "/(.+)" "/$1.html" break;
		root {{ project_root }}/target/html/;
	}
}