module.exports = {
    yes: {
        options: {
            host: "seated.guru",
            username: "deploy",
            password: "deploy_me_pl3a$e_28y3th9gvnweviw"
        },
        files: [{
            cwd: 'target',
            src: '**/*',
            filter: 'isFile',
            dest: '/var/sites/seated.guru'
        }]
    }
};
