module.exports = {
	"build-css": ['sass'],
	"build-js": [],
	"build-nginx": ['nunjucks-alt:nginx'],
	"build-html": ['nunjucks-alt:html'],

	"default": ["clean:most", "generate-variables",
        "copy:all",
        "build-js", "build-html", "build-css",
        "clean:extras"],

    "nginx": ["clean:most", "generate-variables",
        "build-nginx"],

    "dev": ["default", "watch"]

};
