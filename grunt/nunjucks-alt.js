module.exports = function (grunt) {
	var path = require("path");

	return {
		nginx: {
			options: {
				searchPaths: ['nginx/**','target'],
                name: /(?:target|nginx)\/(.*?)\.nunjucks$/
			},
			src: 'nginx/server.conf.nunjucks',
			dest: 'target/nginx/server.conf'
		},
		html: {
			options: {
				searchPaths: ['target/html/**','target'],
				name: /target\/(?:html\/)?(.*?)\.nunjucks/
			},
			expand: true,
			src: 'target/html/*.nunjucks',
			dest: 'target/',
			rename: function (destination, filepath) {
				filepath = filepath.replace('target/', '')
                    .replace('.nunjucks', '.html');
				return path.join(destination, filepath);
			}
		},
		// DOESNT WORK
		markdown: {
			options: {
				searchPaths: ['target/html/**','target'],
				name: /target\/(?:html\/)?(.*?)\.nunjucks/
			},
			expand: true,
			src: 'src/markdown/*',
			dest: 'target/',
			rename: function (destination, filepath) {
				filepath = filepath.replace('target/', '')
					.replace('.nunjucks', '.html');
				return path.join(destination, filepath);
			}
		},
	};
};
