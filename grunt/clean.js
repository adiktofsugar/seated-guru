module.exports = {
	nginx: ['target/nginx'],
	most: ['target/*', '!target/nginx'],
    extras: ['target/scss', 
        'target/css/**/*.scss',
        'target/html/**/*.nunjucks',
        'target/variables.yml', 'target/variables.nunjucks']
};
