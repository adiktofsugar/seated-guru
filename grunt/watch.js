module.exports = {
	html: {
		files: ['src/html/**/*.html'],
		tasks: ['build-html']
	},
	css: {
		files: ['src/scss/**'],
		tasks: ['build-css']
	},
	js: {
		files: ['src/js/**'],
		tasks: ['build-js']
	}
};