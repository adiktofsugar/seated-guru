module.exports = function (grunt) {

    return {
        all: {
            options: {
                extraData: {
                    server_name: grunt.option("server_name") || "dev.seated.guru",
                    project_root: grunt.option("project_root") || process.cwd(),
                    media_version: grunt.option("media_version") || 'dev'
                },
                formatters: {
                    "js": function (sourceObject) {
                        var string = 'define([], function () {' +
                            'return ' + JSON.stringify(sourceObject, null, 4) +
                            '})';
                        return string;
                    },
                    "nunjucks": function (sourceObject) {
                        var string = '';
                        for (var key in sourceObject) {
                            if (sourceObject.hasOwnProperty(key)) {
                                var value = sourceObject[key];
                                string += '{% set ' + key + ' = "' + value + '" %}' + "\n";
                            }
                        }
                        return string;
                    }
                }
            },
            src: 'src/variables.yml',
            dest: [
                'target/scss/_variables.scss',
                'target/js/variables.js',
                'target/variables.nunjucks']
        }
    };
};
