var path = require("path");

module.exports = {
	options: {
		includePaths: ['target/scss']
	},
	all: {
		expand: true,
        cwd: 'target/',
		src: 'scss/*.scss',
		dest: 'target/',
		rename: function (dest, filepath) {
			filepath = filepath
				.replace("scss/", "css/")
				.replace(".scss", ".css");
			return path.join(dest, filepath);
		}
	}
};
