module.exports = function (grunt) {
    return {
        all: {
            expand: true,
            cwd: "src/",
            src: "**/*",
            dest: "target/"
        }
    };
};
