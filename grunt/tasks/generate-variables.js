var path = require("path");
var extend = require("extend");


module.exports = function (grunt) {

    grunt.registerMultiTask("generate-variables",
    "Generates variables in the format necessary for various file types", function () {

        var options = this.options({
            formatters: {},
            typeMap: {},
            extraData: {}
        });

        var formatters = extend({
            "json": function (sourceObject) {
                return JSON.stringify(sourceObject, null, 4);
            },
            "scss": function (sourceObject) {
                var string = "";
                for (var key in sourceObject) {
                    if (sourceObject.hasOwnProperty(key)) {
                        var value = sourceObject[key];
                        string += "$" + key + ": " + '"'+value+'"' + ";\n";
                    }
                }
                return string;
            }
        }, options.formatters);

        var typeMap = extend({
            "sass": "scss"
        }, options.typeMap);

        var getTypeByExt = function (ext) {
            var type = ext.slice(1);
            if (typeMap[type]) {
                return typeMap[type];
            }
            return type;
        }

        this.files.forEach(function (filePair) {

            // There's only one source file.
            var source = filePair.src[0];
            var sourceObject = grunt.file.readYAML(source);

            var extendedSourceObject= extend(sourceObject, options.extraData);
            var templatedSourceObject = {};
            for (var key in extendedSourceObject) {
                templatedSourceObject[key] = grunt.template.process(extendedSourceObject[key], {
                    data: extendedSourceObject
                })
            }

            var destinations = filePair.dest;
            destinations.forEach(function (destination) {
                var ext = path.extname(destination);
                var type = getTypeByExt(ext);

                var formatter = formatters[type];
                if (!formatter) {
                    throw new Error("unsupported extension - " + ext);
                }

                grunt.file.write(destination, formatter(templatedSourceObject));
                grunt.log.ok("Wrote " + destination);
            });
        });
    });
};
